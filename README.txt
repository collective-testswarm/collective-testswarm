Plone product to simplify running JavaScript tests on a TestSwarm instance
==========================================================================

Register the TestSwarm inject.js and provide some helper functions to start the test ZServer, add jobs to the TestSwarm instance and collect results. This should be useable directly for local testing and also from a continuous integration tool such as JenkinsCI.
