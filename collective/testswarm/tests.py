from os import environ
import time
import urllib2

from Products.PloneTestCase import PloneTestCase as ptc

from utils import add_testswarm_job, start_testswarm_zserver

ptc.installProduct('collective.testswarm')
ptc.setupPloneSite(products=('collective.testswarm',),
                   extension_profiles=('collective.testswarm:default',
                                       'collective.testswarm:test'))

class TestZserver(ptc.PloneTestCase):
    def test_start_zserver(self):
        start_testswarm_zserver()
        usock = urllib2.urlopen("http://localhost:55555/plone")
        self.assertEqual(usock.code, 200)
        test_run_time = 20
        print("Test plone server available for %s seconds \n\n"
              "http://localhost:55555/plone/example-qunit-suite"
              % test_run_time)
        time.sleep(test_run_time)

class TestAddJob(ptc.PloneTestCase):
    def test_add_job(self):
        testswarm_url = environ.get("TS_URL", environ.get("ts_url", ""))
        user = environ.get("TS_USER", environ.get("ts_user", ""))
        auth = environ.get("TS_AUTH", environ.get("ts_auth", ""))
        job_name = environ.get("TS_JOB", environ.get("ts_job", ""))
        urls = environ.get("TS_URLS", environ.get("ts_urls", ""))
        suites = environ.get("TS_SUITES", environ.get("ts_suites", ""))
        browsers = environ.get("TS_BROWSERS", environ.get("ts_browsers", ""))

        job_url = add_testswarm_job(testswarm_url, user, auth, job_name, urls,
                                    suites, browsers)
        print("TestSwarm job: %s" % job_url)
