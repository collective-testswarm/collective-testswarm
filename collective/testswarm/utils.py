""" Helper functions to start a test ZServer and add TestSwarm jobs
"""
from urllib import quote_plus
import time
import urllib2

from Products.Five.testbrowser import Browser
from Testing.ZopeTestCase.threadutils import setNumberOfThreads, QuietThread, \
    zserverRunner

def add_testswarm_job(testswarm_url, user, auth, job_name,
                      urls, suites, browsers="popularbeta"):
    """ Add a job to the TestSwarm instance """
    quoted_urls = "&urls[]=".join(quote_plus(url) for url in [urls])
    quoted_suites = "&suites[]=".join(quote_plus(suite) for suite in [suites])
    job_data = ("state=addjob&output=dump&auth="+auth+
               "&user="+user+"&job_name="+quote_plus(job_name)+
               "&browsers="+browsers+"&urls[]="+quoted_urls+
               "&suites[]="+quoted_suites)
    print("Opening %s%s" %(testswarm_url, job_data))
    usock = urllib2.urlopen(testswarm_url, job_data)
    job_id = usock.read()
    return testswarm_url+job_id

def get_testswarm_job_results(url, job_id):
    """ Collect the results of a TestSwarm job """
    raise NotImplementedError

def start_testswarm_zserver(host="localhost", port=55555, log=None):
    '''Starts an HTTP ZServer thread.
    copied from Testing.ZopeTestCase.utils startZServer'''
    setNumberOfThreads(1)
    t = QuietThread(target=zserverRunner, args=(host, port, log))
    t.setDaemon(1)
    t.start()
    time.sleep(0.1)
